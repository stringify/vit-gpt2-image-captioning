import torch
import re
import os
import gradio as gr
from transformers import AutoTokenizer, ViTImageProcessor, VisionEncoderDecoderModel

# 目前启动脚本工作目录未指定为 app.py 所在目录
os.chdir(os.path.dirname(os.path.abspath(__file__)))

device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)
encoder_checkpoint = "hf-models/vit-gpt2-image-captioning"
decoder_checkpoint = "hf-models/vit-gpt2-image-captioning"
model_checkpoint = "hf-models/vit-gpt2-image-captioning"
feature_extractor = ViTImageProcessor.from_pretrained(encoder_checkpoint)
tokenizer = AutoTokenizer.from_pretrained(decoder_checkpoint)
model = VisionEncoderDecoderModel.from_pretrained(model_checkpoint).to(device)


def predict(image,max_length=64, num_beams=4):
  image = image.convert('RGB')
  image = feature_extractor(image, return_tensors="pt").pixel_values.to(device)
  clean_text = lambda x: x.replace('<|endoftext|>','').split('\n')[0]
  caption_ids = model.generate(image, max_length = max_length)[0]
  caption_text = clean_text(tokenizer.decode(caption_ids))
  return caption_text



input = gr.components.Image(label="Upload any Image", type = 'pil', optional=True)
output = gr.components.Textbox(type="text",label="Captions")
examples = [f"./example{i}.jpg" for i in range(1,7)]

title = "图生文"
description = ""
interface = gr.Interface(
        fn=predict,
        description=description,
        inputs = input,
        theme=gr.themes.Glass(),
        outputs=output,
        examples = examples,
        title=title,
        allow_flagging="never"
    )
interface.launch(debug=True)